﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCMusicStore_04.Models
{
    public class MusicStore_04DB : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        //public MusicStore_04DB() : base("name=MusicStore_04DB")
        //{
        //}
        //public MusicStore_04DB() : base("Data Source=.\\SQLEXPRESS;Integrated Security=True;Initial Catalog=MVCMusicStore_06")
        //{
        //}
        public MusicStore_04DB() : base("DefaultConnection")
        {
        }
        public System.Data.Entity.DbSet<MVCMusicStore_04.Models.Album> Albums { get; set; }

        public System.Data.Entity.DbSet<MVCMusicStore_04.Models.Artist> Artists { get; set; }

        public System.Data.Entity.DbSet<MVCMusicStore_04.Models.Genre> Genres { get; set; }

        public System.Data.Entity.DbSet<MVCMusicStore_04.Models.Order> Orders { get; set; }
    }
}
