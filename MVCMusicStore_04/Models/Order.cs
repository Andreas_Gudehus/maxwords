﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MVCMusicStore_04.Validation;
using System.Web.Mvc;


namespace MVCMusicStore_04.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        [Remote("CheckUserName", "Account")]
        public string Username { get; set; }
        [Required]
        [StringLength(10, ErrorMessage = "{0} ist zu lang")]
        [MaxWords(2)]
        public string FirstName { get; set; }
        [MaxWords(3)]
        [Required]
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Dies ist keine E-Mail Adresse")]
        public string Email { get; set; }
        public decimal Total { get; set; }
        //public List<OrderDetail> OrderDetails { get; set; }
    }
}